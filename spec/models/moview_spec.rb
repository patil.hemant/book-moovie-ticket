require 'rails_helper'

RSpec.describe Movie, :type => :model do
  subject {
    described_class.new(name: "Kal Ho Naa Ho",
                        description: "Horror movie",
                        director: 'Rajesh Roshan'
                      )
  }

  it "is valid with valid attributes" do
    expect(subject).to be_valid
  end

  it "is not valid without a name" do
    subject.name = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a description" do
    subject.description = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a director" do
    subject.director = nil
    expect(subject).to_not be_valid
  end

  it "movie name should not be too small" do
    subject.name = "a"
    expect(subject).to_not be_valid
  end

  it "director name should not be too long" do
    subject.name = "a" * 25
    expect(subject).to_not be_valid
  end

end
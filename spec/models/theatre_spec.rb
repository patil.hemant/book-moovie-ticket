require 'rails_helper'

RSpec.describe Theatre, :type => :model do
  subject {
    described_class.new(name: "Cine Multiplex",
                        address: "Oune, hinjwali",
                        contact_no: '9878767898'
                      )
  }

  it "is valid with valid attributes" do
    expect(subject).to be_valid
  end

  it "is not valid without a name" do
    subject.name = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a address" do
    subject.address = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without a contact_no" do
    subject.contact_no = nil
    expect(subject).to_not be_valid
  end

  it 'is invalid with contact no having aphabates' do
	  expect(FactoryGirl.create(:theatre, name: 'Cine Multiplex', address: 'Baner, Pune', contact_no: '987A3546W38').save).to be_falsey
	end	

end
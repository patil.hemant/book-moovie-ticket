require 'rails_helper'
include Warden::Test::Helpers


describe BookingsController do

  describe "Index" do

    it "should allow user to view the list of available datetime slots for given movie" do
      user = FactoryGirl.create(:user, email: 'hemant@gmail.com', password: "password")
      get :index, {}, sign_in(user)
      expect(response).to redirect_to(bookings_path(id: 2))
    end

    it "should not list available datetime slots if the user is not logged in" do
      get :index
      expect(response.status).to eq(302)
      expect(response).to redirect_to(new_user_sessions_path)
    end
  end

end
  
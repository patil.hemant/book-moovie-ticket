require 'rails_helper'
  describe Booking do
    
    before do
      user = FactoryGirl.create(:user, email: 'hemant@gmail.com', password: "password")
      log_in user.email, "password"
    end
    
    describe "GET /my-bookings" do
      it "should show a list of all movie tickets booked" do
        visit my_bookings_path
        expect(page).to have_content('All Bookings')
      end
    end
    
    scenario 'GET /new' do
      def goto_book_ticket
        visit root_path
        expect(page).to have_content('Refresh your mood by watching Hollywood & Bollywood movies.')
        click_link 'Book ticket'
        expect(page).to have_content('Book your slot')
      end

      describe 'shows the firt step of booking as book your slot' do
        goto_book_ticket
      end
      
      describe 'shows the new account creation form' do
        goto_book_ticket
        click_link "Let's Select Seats"
        expect(page).to have_content('SCREEN - All Eyes This Way Please')
      end
      
      describe 'allows to choose screen and seats and book ticket by clicking on confirm my booking button' do
        goto_book_ticket
        click_link "Let's Select Seats"
        expect(page).to have_content('SCREEN - All Eyes This Way Please')
        within '#booking-form' do
          fill_in '#seat_numbers', with: [135,233]
          expect(page).to have_selector('.confirm-my-booking', visible: true)
          click_button 'Confirm My Booking'
      end
    end
  end
end
class Admin::BookingsController < ApplicationController
  before_action :authenticate_user!

  def index
	 @bookings = Booking.order('created_at desc').includes(:show, :movie, :customer, {:booked_seats => {:seat => {:row => :screen}} }).paginate(page: params[:page], per_page: 5)
  end
  
  def reports
  	if params[:date].present?
  		date = params[:date].to_date
  		@bookings = Booking.where('booking_for_date = ?', date).order('created_at desc').includes(:show, :movie, :customer, {:booked_seats => {:seat => {:row => :screen}} })
  	end
  end
end

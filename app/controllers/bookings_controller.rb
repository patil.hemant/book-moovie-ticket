class BookingsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_movie, only: [:index, :create]

  def index
    if params[:step] == '2'
      @shows = @movie.shows.where(date: params[:date].to_date)
      @screens = Theatre.last.screens.order('price ASC').includes(:rows => :seats)
      get_available_seats
    else
      @shows = @movie.shows.group_by{|e|e.date}
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def my_bookings
    @bookings = current_user.bookings.order('created_at DESC').includes(:show, :movie, :customer, {:booked_seats => {:seat => {:row => :screen}} }).paginate(page: params[:page], per_page: 5)
  end

  
  def get_available_seats
    booking_date = params[:date].to_date
    time_slot_id = params[:ts_id]
    show = @movie.shows.where(time_slot_id: params[:ts_id], date: params[:date].to_date).last
    @bookings = Booking.where(movie_id: @movie.id, booking_for_date: booking_date, show_id: show.id).includes(:movie, :booked_seats)
    @booked_seats = []
    if @bookings.present?
      @bookings.each do |booking|
        booking.booked_seats.each do |seat|
          @booked_seats << seat.seat_id
        end
      end
    end
  end

  def new

  end

  def create
    if check_valid_booking
      show = @movie.shows.where(time_slot_id: params[:time_slot_id], date: params[:booking_for_date].to_date).last
      @booking = Booking.create(
                      movie_id: @movie.id,
                      show_id: show.id,
                      booking_for_date: params[:booking_for_date].to_date,
                      customer_id: current_user.id,
                    )
      params[:seat_numbers].split(',').each do |seat_id|
        @booking.booked_seats.create(seat_id: seat_id)
      end
    else
      @cant_book = true
      @err_msg = "Sorry can't book, some of the seats just got booked. Please choose other seats."
    end  
    respond_to do |format|
      format.js
    end
  end

  def show
    @booking = Booking.find(params[:id])
    @movie = @booking.movie
  end

  def check_valid_booking
    true #logic to check if at the same time no other customer has booked seats selected by current customer
  end

  def edit

  end

  def update

  end

  def destroy

  end

  def donwload_invoice
    @booking = Booking.find(params[:id])
    @movie = @booking.movie
    respond_to do |format|
      format.html
      format.pdf do
       render pdf: "#{@movie.name.downcase}-invoice",
       template: "bookings/invoice.pdf.erb",
       :disposition => 'attachment',
       layout: nil
     end
    end
  end

  private

  def set_movie
    @movie = Movie.find(params[:id])
  end

end

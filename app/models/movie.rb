class Movie < ApplicationRecord
	serialize :star_casts, Hash
	mount_uploader :banner, BannerUploader

	has_many :shows

	validates :name, presence: true, length: { in: 6..25 }
	validates :description, presence: true
	validates :director, presence: true, length: { in: 5..20 } 

end

class Theatre < ApplicationRecord
	has_many :seat_types
	has_many :time_slots
	has_many :screens

	validates :name, presence: true, length: { in: 6..25 }
	validates :address, presence: true
	validates :contact_no, presence: true 
	validates :contact_no, format: { with: /\A\d+\z/, message: "Integer only." }

end

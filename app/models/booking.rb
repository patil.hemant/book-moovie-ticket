class Booking < ApplicationRecord
	belongs_to :show
	belongs_to :movie
	belongs_to :customer , :class_name => 'User'
	has_many :booked_seats

	def total_amount
		self.booked_seats.map{|booked_seat|booked_seat.seat.row.screen.price}.sum
	end
	
end

class SeatType < ApplicationRecord
	belongs_to :theatre
	has_many :seats
end


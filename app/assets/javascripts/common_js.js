function initDatatables(sort_column, order, columnWidth, callBackFunction) {
    $(function () {
        columnWidth = columnWidth || [{

            "defaultContent": "-",
            "targets": "_all"
        }]
        console.log(columnWidth)
        table = $('#generic-data-table').DataTable({
            "pagingType": "full_numbers",
            iDisplayLength: 100,
            bJqueryUI: true,
            bServerSide: true,
            processing: true,
            "language": {
                "processing": "<div></div><div></div><div></div><div></div><div></div>"
            },
            sAjaxSource: $('#generic-data-table').data('source'),
            "order": [
                [sort_column, order]
            ],
            "columnDefs": columnWidth,
            dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
         "<'row'<'col-sm-12'tr>>" +
         "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        //  "initComplete": function() {
            //  if(typeof callBackFunction === "function"){
            //      callBackFunction()
            //  }
        //  },
         "drawCallback": function( settings ) {
            if(typeof callBackFunction === "function"){
                callBackFunction()
            }
        }
            
        });

        // table.on( 'search.dt', function () {
        //     if(typeof callBackFunction === "function"){
        //         console.warn(callBackFunction)
        //         callBackFunction()
        //     }
        // } );

    })
}
function initDatatablesUsers(sort_column, order, columnWidth) {
    $(function () {
        columnWidth = columnWidth || []
        console.log(columnWidth)
        table = $('#generic-data-table').DataTable({
            "pagingType": "full_numbers",
            iDisplayLength: 10,
            bJqueryUI: true,
            bServerSide: true,
            sAjaxSource: $('#generic-data-table').data('source'),
            "order": [
                [sort_column, order]
            ],
            "columnDefs": columnWidth
        });

    })
}

function initDropify(){
    $('.dropify').dropify();
    var drEvent = $('#input-file-events').dropify();

    drEvent.on('dropify.beforeClear', function(event, element) {
        return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
    });

    drEvent.on('dropify.afterClear', function(event, element) {
        alert('File deleted');
    });

    drEvent.on('dropify.errors', function(event, element) {
        console.log('Has Errors');
    });

    var drDestroy = $('#input-file-to-destroy').dropify();
    drDestroy = drDestroy.data('dropify')
    $('#toggleDropify').on('click', function(e) {
        e.preventDefault();
        if (drDestroy.isDropified()) {
            drDestroy.destroy();
        } else {
            drDestroy.init();
        }
    })
}
class CreateBookings < ActiveRecord::Migration[5.2]
  def change
    create_table :bookings do |t|
      t.integer :show_timing_id
      t.date :booking_for_date
      t.integer :movie_id
      t.integer :customer_id

      t.timestamps
    end
    add_index :bookings, :show_timing_id
    add_index :bookings, :movie_id
    add_index :bookings, :customer_id
  end
end

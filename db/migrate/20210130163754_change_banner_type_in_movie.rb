class ChangeBannerTypeInMovie < ActiveRecord::Migration[5.2]
  def change
  	change_column :movies, :banner, :text
  end
end

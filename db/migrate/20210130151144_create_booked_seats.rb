class CreateBookedSeats < ActiveRecord::Migration[5.2]
  def change
    create_table :booked_seats do |t|
      t.integer :booking_id
      t.integer :seat_id

      t.timestamps
    end
    add_index :booked_seats, :booking_id
    add_index :booked_seats, :seat_id
  end
end

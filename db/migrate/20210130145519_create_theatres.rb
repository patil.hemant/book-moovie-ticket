class CreateTheatres < ActiveRecord::Migration[5.2]
  def change
    create_table :theatres do |t|
      t.string :name
      t.text :address
      t.string :contact_no

      t.timestamps
    end
  end
end

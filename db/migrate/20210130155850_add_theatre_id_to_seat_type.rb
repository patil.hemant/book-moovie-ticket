class AddTheatreIdToSeatType < ActiveRecord::Migration[5.2]
  def change
    add_column :seat_types, :theatre_id, :integer
    add_index :seat_types, :theatre_id
  end
end

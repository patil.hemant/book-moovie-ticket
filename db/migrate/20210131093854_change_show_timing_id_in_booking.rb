class ChangeShowTimingIdInBooking < ActiveRecord::Migration[5.2]
  def change
  	    rename_column :bookings, :show_timing_id, :show_id

  end
end

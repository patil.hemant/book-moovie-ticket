class CreateRows < ActiveRecord::Migration[5.2]
  def change
    create_table :rows do |t|
      t.string :row_no
      t.integer :screen_id

      t.timestamps
    end
    add_index :rows, :screen_id
  end
end

class CreateMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :movies do |t|
      t.string :name
      t.string :banner
      t.text :description
      t.string :director
      t.text :star_casts
      t.integer :minutes
      t.string :language

      t.timestamps
    end
  end
end

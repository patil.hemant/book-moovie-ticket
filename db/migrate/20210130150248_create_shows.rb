class CreateShows < ActiveRecord::Migration[5.2]
  def change
    create_table :shows do |t|
      t.integer :movie_id
      t.date :date
      t.integer :time_slot_id

      t.timestamps
    end
    add_index :shows, :movie_id
    add_index :shows, :time_slot_id
  end
end

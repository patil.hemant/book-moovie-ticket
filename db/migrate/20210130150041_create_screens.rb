class CreateScreens < ActiveRecord::Migration[5.2]
  def change
    create_table :screens do |t|
      t.string :name
      t.integer :theatre_id
      t.float :price

      t.timestamps
    end
    add_index :screens, :theatre_id
  end
end

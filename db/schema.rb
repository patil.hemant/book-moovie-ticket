# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_31_093854) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "booked_seats", force: :cascade do |t|
    t.integer "booking_id"
    t.integer "seat_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["booking_id"], name: "index_booked_seats_on_booking_id"
    t.index ["seat_id"], name: "index_booked_seats_on_seat_id"
  end

  create_table "bookings", force: :cascade do |t|
    t.integer "show_id"
    t.date "booking_for_date"
    t.integer "movie_id"
    t.integer "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_bookings_on_customer_id"
    t.index ["movie_id"], name: "index_bookings_on_movie_id"
    t.index ["show_id"], name: "index_bookings_on_show_id"
  end

  create_table "movies", force: :cascade do |t|
    t.string "name"
    t.text "banner"
    t.text "description"
    t.string "director"
    t.text "star_casts"
    t.integer "minutes"
    t.string "language"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rows", force: :cascade do |t|
    t.string "row_no"
    t.integer "screen_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["screen_id"], name: "index_rows_on_screen_id"
  end

  create_table "screens", force: :cascade do |t|
    t.string "name"
    t.integer "theatre_id"
    t.float "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["theatre_id"], name: "index_screens_on_theatre_id"
  end

  create_table "seat_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "theatre_id"
    t.index ["theatre_id"], name: "index_seat_types_on_theatre_id"
  end

  create_table "seats", force: :cascade do |t|
    t.string "seat_no"
    t.integer "row_id"
    t.integer "seat_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["row_id"], name: "index_seats_on_row_id"
    t.index ["seat_type_id"], name: "index_seats_on_seat_type_id"
  end

  create_table "shows", force: :cascade do |t|
    t.integer "movie_id"
    t.date "date"
    t.integer "time_slot_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["movie_id"], name: "index_shows_on_movie_id"
    t.index ["time_slot_id"], name: "index_shows_on_time_slot_id"
  end

  create_table "theatres", force: :cascade do |t|
    t.string "name"
    t.text "address"
    t.string "contact_no"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "time_slots", force: :cascade do |t|
    t.string "start_time"
    t.string "end_time"
    t.integer "theatre_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["theatre_id"], name: "index_time_slots_on_theatre_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "role"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end

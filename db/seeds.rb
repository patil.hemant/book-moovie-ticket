# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(email: 'admin@gmail.com', password: 'password', role: 'Admin')

theatre = Theatre.create(name: 'Metro Junction', address: 'Baner, Pune, 450 956', contact_no: '022-432645')

sample_movies = ['Tere Naam', 'Ishq', 'Kal Ho Naa Ho', 'Sairat', 'Kabhi Khushi Kabhi Gham', 'Hum Apke Hain Kaun']

time_slots = [{start_time: '12:00 PM', end_time: '03:00 PM'}, {start_time: '03:00 PM', end_time: '06:00 PM'}, {start_time: '06:00 PM', end_time: '09:00 PM'}, {start_time: '09:00 PM', end_time: '12:00 AM'}]
time_slots.each do |slot|
	theatre.time_slots.create(
										start_time: slot[:start_time],
										end_time: slot[:end_time]	
									)
end

sample_movies.each do |movie_name|
	movie = Movie.create(
								name: movie_name,
								description: Faker::Lorem.paragraph_by_chars(number: 200),
								director: ['Madhur Bhandarkar', 'Ashuotsh Rana', 'Rakesh Roshan'].sample,
								star_casts: [{male: ['Hrithik Roshan', 'Amir Khan'], female: ['Madhuri Dixit', 'Rimi Sen']},{male: ['Akshay Kumar', 'Amir Khan'], female: ['Amisha Patel', 'Madhuri Dixit']}].sample,
								minutes: [90, 100, 120, 180].sample,
								language: ['Marathi','Hindi', 'English'].sample,
								banner: File.open("#{Rails.root}/app/assets/images/#{['banner1.jpg','banner2.jpg', 'banner3.jpg'].sample}")
							)
end

dates =	[Date.today, Date.today + 1, Date.today + 2, Date.today + 3, Date.today + 4]
dates.each do |date|
	TimeSlot.all.each do |time_slot|
		Show.create(date: date, time_slot_id: time_slot.id, movie_id: Movie.pluck(:id).sample)
	end
end

screens = ['Bronze', 'Silver', 'Gold']

seat_types = ['Standard', 'Upper', 'Lower']
seat_types.each do |seat|
	theatre.seat_types.create(name: seat)
end

seat_type_ids = SeatType.pluck(:id)
screens.each do |screen|
	price = ((screen == 'Bronze') ? 120 : ((screen == 'Silver') ? 160 : 180))
	screen_obj = theatre.screens.create(name: screen, price: price)
	['A', 'B', 'C'].each do |row_no|
		row = screen_obj.rows.create(row_no: row_no)
		10.times do |index|
			row.seats.create(
												seat_no: index + 1,
												seat_type_id: seat_type_ids.sample
											)
		end
	end	if screen == 'Bronze'
	['D', 'E', 'F', 'G', 'H', 'I','J','K'].each do |row_no|
		row = screen_obj.rows.create(row_no: row_no)
		10.times do |index|
			row.seats.create(
												seat_no: index + 1,
												seat_type_id: seat_type_ids.sample
											)
		end
	end	if screen == 'Silver'
	['L', 'M', 'N', 'O', 'P'].each do |row_no|
		row = screen_obj.rows.create(row_no: row_no)
		10.times do |index|
			row.seats.create(
												seat_no: index + 1,
												seat_type_id: seat_type_ids.sample
											)
		end
	end	if screen == 'Gold'
end


